docker run \
	-p 8000:8000 -p 35729:35729 \
	--name docker_presentation \
	-v /Users/jaerts/Presentations/MyOwn/YAC20160601/images:/opt/presentation/images \
	-v /Users/jaerts/Presentations/MyOwn/YAC20160601/slides:/opt/presentation/slides \
	-d jandot/docker-presentation
